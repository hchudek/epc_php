<?php
ob_start("ob_gzhandler");									
header("Content-Type: text/javascript; charset=UTF-8"); 
?>

$search.configuration = {
   checkplant: true,
   checkresin: true,
}
if(typeof $_request.mtype == "string") {
   if($_request.mtype == "zvrp") {
	$search.configuration.checkresin = false;
	$search.configuration.checkwc = false;
   }
}

$search.embed.domino = function(r) {
   intdt = document.querySelector("[form='add_tool']").querySelectorAll("td[tn]");
   toollist = [];
   for(k in intdt) if(typeof intdt[k] == "object") {
      if(intdt[k].getAttribute("tn") != "") toollist.push(intdt[k].getAttribute("tn"));
   }
   h = ["Tool number", "Tool description", "Plant", "Workcenter"];
   table = document.createElement("table");
   table.setAttribute("form", "toollist");
   tr = document.createElement("tr");
   table.appendChild(tr);
   for(e in h) {
      th = document.createElement("th");
      th.textContent = h[e];
      tr.appendChild(th);
   }
   for(e in r) {  
      i = document.querySelector("[form='$search.input']");
      v = i.value.trim().toLowerCase();
      if(r[e]["number"].toLowerCase().indexOf(v) > -1) {
         tr = document.createElement("tr");
         table.appendChild(tr);
         for(k in r[e]) {
            if(k != "unid" && k != "selectable") {
               td = document.createElement("td");
               use_href = (document.querySelector("[form='$search.inject']").getAttribute("open") == "domino") ? "?createdocument&open=domino&p=finalize&"  : "finalize.php?";
//               td.innerHTML = (k == "number" && r[e].selectable == "1") ? "<a href=\"" + use_href + "tool=" + r[e].unid + "&part=" + i.getAttribute("pu").replace(/%%PART%%/, "") + "&t=" + i.getAttribute("t") + "\">" + unescape(r[e][k]) + "</a>" :  unescape(r[e][k]);
               td.innerHTML = (k == "number" && (r[e].selectable == "1" || $_request.mtype == "zvrp")) ? "<a href=\"" + use_href + "mtype=" + cache.mtype + "&tool=" + r[e].unid + "&part=" + i.getAttribute("pu").replace(/%%PART%%/, "") + "&t=" + i.getAttribute("t") + "\">" + unescape(r[e][k]) + "</a>" :  unescape(r[e][k]);
               if(k == "number" && r[e].selectable != "1" && $search.configuration.checkresin) {
                  td.innerHTML = $search.embed.template.resin(td.innerHTML, r[e].unid); 
               }
               else {
                  if(td.firstChild) {
                     el = td.firstChild;
                     if(String(el.tagName).toLowerCase() == "a") {
                        el.setAttribute("plant", unescape(r[e].plant));
                        el.setAttribute("workcenter", unescape(r[e].workcenter));
                        el.style.position = "relative";
                        el.style.left = "-36px";
                        img = document.createElement("img");
                        img.setAttribute("style", "display:inline-block; width:22px; height:11px; position:relative; left: -34px; margin-right:14px;");
                        img.src = (in_array(el.getAttribute("plant"), active.maintplant)) ? "logo_sap_s.gif" : "blank.gif";
                        td.insertBefore(img, el);
                        $search.embed.template.validate(el, false);
                     }
                  }
               }
               tr.appendChild(td);
            }
         }
      }
   }
   with(document.querySelector("[form='$search.inject']")) {
      innerHTML = "";
      appendChild(table);
   }
}
$search.embed.template = {
   resin: function(h, unid) {
      c = document.createElement("c");
      c.setAttribute("style", "cursor:pointer; color:rgb(129,129,129);");
      c.setAttribute("onmouseover", "this.style.textDecoration = 'underline'; this.nextSibling.style.display = 'inline-block';");
      c.setAttribute("onmouseout", "this.style.textDecoration = 'none'; this.nextSibling.style.display = 'none';");
      c.setAttribute("onclick", "location.href = '0/" + unid + "?editdocument&key=tool&r=" + escape(location.href) + "'");
      c.innerHTML =  h;
      div = $search.alert("Resin not set! Click to edit!", 10);
      return c.outerHTML + div.outerHTML;
   },
   validate: function() {
      el = arguments[0];
      l = (typeof arguments[1] == "undefined") ? true : false; 
      plant = (el.hasAttribute("plant")) ? el.getAttribute("plant") : "";
      wc = (el.hasAttribute("workcenter")) ? el.getAttribute("workcenter") : "";
      if(el.textContent.toLowerCase().indexOf("no tool") == -1) {
         if(el.textContent.length < 16) {
            div = $search.alert("No SAP number!", -25);
            el.style.textDecoration = "none";
            el.style.color = "rgb(129,129,129)";
            el.setAttribute("href", "javascript:void(0);"); 
            el.parentNode.appendChild(div);
            textdecoration = (l) ? "underline" : "none";
            el.setAttribute("textdecoration", textdecoration);
            el.onmouseover = function() { this.nextSibling.style.display = "inline-block"; this.style.textDecoration = this.getAttribute("textdecoration"); }
            el.onmouseout = function() { this.nextSibling.style.display = "none"; this.style.textDecoration = "none"; }
            el.onclick = function() { o = document.querySelector("[form='$search.input']"); o.value = (this.textContent).trim().substr(0, 10); $search.execute(o, false); }
         }
         else {
            el.setAttribute("onclick", "$search.check_tdt_status(this, '" + el.getAttribute("href") + "');");
            el.setAttribute("href", "javascript:void(0);");
         }
      }
      if(in_array(plant, workcenter.plant)) {
         if(wc == "" && $_request.mtype != "zvrp") {
            div = $search.alert("Workcenter unknown!");
            el.style.textDecoration = "none";
            el.style.color = "rgb(129,129,129)";
            el.removeAttribute("href");
            el.parentNode.appendChild(div);
            el.onmouseover = function() { this.nextSibling.style.display = "inline-block"; }
            el.onmouseout = function() { this.nextSibling.style.display = "none"; }
            el.setAttribute("error", "workcenter");
         }
      }
      else {
         if(plant.trim() == "" && el.textContent.toLowerCase().indexOf("no tool") == -1 && $search.configuration.checkplant) {
            div = $search.alert("Plant missing!", -25);
            el.style.textDecoration = "none";
            el.style.color = "rgb(129,129,129)";
            if(l) el.setAttribute("href", "javascript:void(0);"); else el.removeAttribute("href");
            el.parentNode.appendChild(div);
            textdecoration = (l) ? "underline" : "none";
            el.setAttribute("textdecoration", textdecoration);
            el.onmouseover = function() { this.nextSibling.style.display = "inline-block"; this.style.textDecoration = this.getAttribute("textdecoration"); }
            el.onmouseout = function() { this.nextSibling.style.display = "none"; this.style.textDecoration = "none"; }
            if(l) el.onclick = function() { o = document.querySelector("[form='$search.input']"); o.value = (this.textContent).trim().substr(0, 10); $search.execute(o, false); }
         }
      }
      if(!l) {
         if(in_array(el.textContent, toollist)) {
            div = $search.alert("Already listed above!");
            el.style.textDecoration = "none";
            el.style.color = "rgb(129,129,129)";
            el.removeAttribute("href");
            el.parentNode.appendChild(div);
            el.onmouseover = function() { this.nextSibling.style.display = "inline-block"; }
            el.onmouseout = function() { this.nextSibling.style.display = "none"; }
            el.onclick = function() { return false; }
         }
      }
   },
   run: function() {
      d = document.querySelectorAll("[anchor='tool']");
      for(k in d) {
         if(typeof d[k] == "object") {
            if(d[k].getAttribute("resin") == "0" && $search.configuration.checkresin) {
               d[k].removeAttribute("href");
               d[k].outerHTML = $search.embed.template.resin(d[k].innerHTML, d[k].getAttribute("unid"));
            }
            else {
               d[k].style.position = "relative";
               d[k].style.left = "-36px";
               img = document.createElement("img");
               img.setAttribute("style", "display:inline-block; width:22px; height:11px; position:relative; left: -34px; margin-right:14px;");
               img.src = (in_array(d[k].getAttribute("plant"), active.maintplant)) ? "logo_sap_s.gif" : "blank.gif";
               d[k].parentNode.insertBefore(img, d[k]);
               $search.embed.template.validate(d[k]);
            }
         }
      }
   }
}

$search.alert = function() {
   txt = arguments[0];
   s = (typeof arguments[1] == "undefined") ? 0 : arguments[1];
   div = document.createElement("div");
   div.setAttribute("style", "position:absolute; display:none;");
   span = document.createElement("span");
   span.setAttribute("style", "display:inline-block; position:relative; left:" + String(-10 + s) + "px; top:-4px; background:rgba(255,255,255,0.92); border:solid 1px rgb(169,169,169); border-radius:3px; padding:4px; box-shadow: 6px 6px 5px -3px #a1a1a1; white-space:nowrap;");
   span.innerHTML = "&nbsp;" + txt.replace(/ /g, "&nbsp;") + "&nbsp;";
   div.appendChild(span);
   return div;
}


$search.check_tdt_status = function() {
 
   try { if(el.hasAttribute("error")) return false; } catch(e) {}
   if(typeof arguments[1] == "boolean") {
      url = $search.url;
      if(typeof arguments[0] == "object") {
         url += "&pot_status=" + escape(arguments[0].pot_status);
         if(arguments[0].pot_status == "100") { 
            if(typeof arguments[2] == "undefined") arguments[2] = "Unknown";
            html = "<span>This POT is grabbed by " + unescape(arguments[2]) + " (T&PM).</span>" +
            "<div style=\"padding-top:70px;\">" + 
            "<button type=\"button\" onclick=\"location.href = '" + unescape(arguments[0].link) + "';\">GO TO TDTRACKING</button>&nbsp;&nbsp;" +  
            "<button type=\"button\" onclick=\"location.href = '" + url + "';\">PROCEED WITH SPECIAL RELEASE</button>&nbsp;&nbsp;" + 
            "<button type=\"button\" onclick=\"infobox_handle.close(true, 'confirm_box');\">CANCEL</button>" + 
            "</div>";
            infobox("POT grabbed", html, 540, 154);
            document.getElementById("confirm_box").firstChild.nextSibling.style.overflow = "hidden";
         }
         else { location.href = url; }
      }
      else { location.href  = url; } 
   }
   else {
      el = arguments[0];
      if(el.hasAttribute("key")) {
         $search.url = arguments[1];
         script = document.createElement("script");
         script.src = "../tdtracking.nsf/v.link_by_key?open&count=1&restricttocategory=" + escape(el.getAttribute("key").toLowerCase().trim());
         document.getElementsByTagName("head")[0].appendChild(script);
         console.log(script.src);
      }
      else location.href = arguments[1];
   }
}

$search.embed.template.run();


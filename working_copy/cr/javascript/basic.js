<?php
ob_start("ob_gzhandler");									
header("Content-Type: text/javascript; charset=UTF-8"); 
session_start();
require_once("../config.php");
?>
window.onload = function() {
   script = document.createElement("script");
   script.src = "<?php print $_SESSION["database_path"]."javascript/".$_SERVER["QUERY_STRING"].".js"; ?>";
   document.getElementsByTagName("head")[0].appendChild(script);
   (function() {
      b = document.getElementsByTagName("body")[0];
      if(b.getAttribute("p") == "add_tool" && b.getAttribute("part") == "") err = "Error! Part lost!";
      if(typeof err == "string") {
         a = document.createElement("a");
         a.href = "?open=domino";
         a.style = "color:rgb(0,102,158); text-decoration:none";
         a.onmouseover = function() { this.style.textDecoration = "underline"; }
         a.onmouseout = function() { this.style.textDecoration = "none"; }
         a.textContent = err;
         with(document.getElementById("PHP")) { innerHTML = ""; appendChild(a); }
      }
   })();
   if(typeof $_request != "undefined") {
      si = document.querySelector("[form='$search.input']");
      if(typeof $_request["search"] == "string" && si) {
         if(typeof $_request["search"] == "string") {
            si.value = $_request.search;
            $search.execute(si, false);
         }
      }
   }
   (function() {
      div = [document.createElement("div"), document.createElement("div")];
      div[0].id = "run_ajax";
      div[0].style = "position: absolute; top: 0px; left:0px; width: 100%; height: 100%; display: none; z-index: 9999; background: rgb(0,0,0);";
      div[1].id = "te_footer";
      div[1].style = "position: absolute; top: " + ($(window).height() - 30) + "px; left: " + ($(window).width() - 32) + "px";
      with(document.getElementsByTagName("body")[0]) { appendChild(div[0]); appendChild(div[1]); }
   })();
}

in_array = function() {
   r = false;
   for(arg in arguments[1]) {
      if(arguments[0] == arguments[1][arg]) {
         r = true;
         break;
      }
   }
   return r;
}

$search = {
   pause: 900,
   timer: 0,
   embed: {},
   execute: function() {
      e = arguments[0];
      t = document.querySelector("[form='$search.inject']");
      if(arguments[1]) {
         v = e.value.toLowerCase().split(" ");
         url = (t.getAttribute("include") == "oracle") ? "get_part.php?q=" + escape(v) : "get_tool.php?q=" + escape(v);
         url = t.getAttribute("path") + url;
         if(document.getElementById("jsonp")) document.getElementById("jsonp").parentNode.removeChild(document.getElementById("jsonp"));
         script = document.createElement("script");
         script.id = "jsonp";
         script.src = url;
         script.onload = function() { eval("$search.embed." + document.querySelector("[form='$search.inject']").getAttribute("include") + "(r)"); }
         document.getElementsByTagName("head")[0].appendChild(script);
      }
      else {
         $search.timer = new Date().getTime();
         window.setTimeout(function() {
            n = new Date().getTime();
            if(n - $search.timer > $search.pause - 90) $search.execute(e, true);
         }, $search.pause);
      }
   },
}

<?php 
if($_SERVER["QUERY_STRING"] == "add_tool") {
   include_once("../../../../../library/tools/javascript/global/infobox.js");   
   print "\r\n\r\n";
   include_once("../../../../../library/tools/javascript/global/do_fade.js");
   print "\r\n\r\n";
   include_once("../../../../../library/tools/javascript/jquery-1.11.1.min.js");
   print "\r\n\r\n";
   include_once("../../../../../library/tools/javascript/jquery-ui.js");
   print "\r\n\r\n";
}
?>

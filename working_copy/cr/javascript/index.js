<?php
		ob_start("ob_gzhandler");									
header("Content-Type: text/javascript; charset=UTF-8"); 
?>
$search.embed.oracle = function(r) {
	h = ["Part number", "Part rev.", "Drw. no.", "Drw. rev.", "Mat. type", "Part description"];
	table = document.createElement("table");
	table.setAttribute("form", "partlist");
	tr = document.createElement("tr");
	table.appendChild(tr);
	for(e in h) {
		th = document.createElement("th");
		th.textContent = h[e];
		tr.appendChild(th);
	}
	for(e in r) {  
		tr = document.createElement("tr");
		table.appendChild(tr);
		for(k in r[e]) {
			td = document.createElement("td");
			use_href = (document.querySelector("[form='$search.inject']").getAttribute("open") == "domino") ? "?open=domino&p=add_tool&"  : "add_tool.php?";
			if(k == "tyco_electronics_corp_part_nbr") {
				if(r[e].material_type_cde == "ZROH") {
					use_href = "javascript:void(0);";
					td.innerHTML = "<a href=\"" + use_href + "\" style=\"cursor: default; text-decoration: none; color: rgb(127,127,127);\" onmouseover=\"this.nextSibling.style.display='inline-block'\" onmouseout=\"this.nextSibling.style.display='none'\">" + r[e][k] + "</a>";
					td.appendChild($search.alert("ZROH"));
				}
				else {
					// Try to get the part document or at least the supplier from the part
					use_href += "part=" + escape((r[e]["tyco_electronics_corp_part_nbr"]  + "+" + r[e]["part_rvs_letter_cde"]).toLowerCase()) + "&orev=" + escape(r[e]["drawing_revision_letter_cde"].toLowerCase()) + "&rev=" + escape(r[e]["drawing_revision_letter_cde"].toLowerCase()) + "&drw=" + escape(r[e]["drawing_nbr"].toLowerCase()) + "&mtype=" + escape(r[e]["material_type_cde"].toLowerCase()) +  "&orig=";
					td.innerHTML = "<a href=\"" + use_href + "\">" + r[e][k] + "</a>";
				}
			}
			else td.innerHTML = r[e][k];
			tr.appendChild(td);
		}
	}
	with(document.querySelector("[form='$search.inject']")) {
		innerHTML = "";
		appendChild(table);
	}
}


$search.alert = function() {
	txt = arguments[0];
	s = (typeof arguments[1] == "undefined") ? 0 : arguments[1];
	div = document.createElement("div");
	div.setAttribute("style", "position:absolute; display:none;");
	span = document.createElement("span");
	span.setAttribute("style", "display:inline-block; position:relative; left:" + String(-10 + s) + "px; top:-4px; background:rgba(255,255,255,0.92); border:solid 1px rgb(169,169,169); border-radius:3px; padding:4px; box-shadow: 6px 6px 5px -3px #a1a1a1; white-space:nowrap;");
	span.innerHTML = "&nbsp;" + txt.replace(/ /g, "&nbsp;") + "&nbsp;";
	div.appendChild(span);
	return div;
}


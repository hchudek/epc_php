<?php
session_start();
require_once("config.php");
require_once("../../../../library/tools/addin_xml.php");
ob_start("ob_gzhandler");									
header("Content-Type: text/html; charset=UTF-8"); 
if($_REQUEST["open"] != "domino") {
   $file = trim(file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/a.rms?open&part=".rawurlencode(strtoupper($_REQUEST["part"]))."&tool=".rawurlencode(strtoupper($_REQUEST["tool"]))));
   eval($file);
   $useform = (in_array($plant, $maintplant)) ? "rms" : "creport";
   if($_REQUEST["maintplant"] == "") {
      $url = $_SESSION["remote_domino_path_main"]."/".$useform."?open&parent=".$part."&loop=&key=part&tool=".strtoupper($_REQUEST["tool"]);
      header("Location:".$url);
   }
}
?>

<!DOCTYPE html>
<html>
<head>
<?php
print "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://".$_SERVER["SERVER_NAME"]."/library/css5/include3.css?".base64_encode("te.woff")."\"></link>\r\n";  
print "<link rel=\"stylesheet\" type=\"text/css\" href=\"".$_SESSION["database_path"]."css/".basename(__FILE__, ".php").".css\"></link>\r\n";  
print "<script src=\"".$_SESSION["database_path"]."javascript/basic.js\"></script>\r\n";
?>
<script>
window.onload = function() {
<?php
if($_REQUEST["open"] != "domino") {
  $maintplant = file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/p.maintplant");
  print "   maintplant = ".str_replace("\n", "", $maintplant).";\r\n";
}
?>
   part = "<?php print strtoupper($_REQUEST["part"]); ?>";
   tool = "<?php print strtoupper($_REQUEST["tool"]); ?>";
   pot_status = "<?php print strtoupper($_REQUEST["pot_status"]); ?>";
   supp = "<?php print strtolower($_REQUEST["sup"]); ?>";
   supplier = "<?php print ($_REQUEST["supplier"]); ?>";
   <?php
   if($_REQUEST["open"] != "domino") {
      print "url = \"".$_SESSION["remote_domino_path_main"]."/rms?open&plant=%%PLANT%%&parent=\" + part.toUpperCase() + \"&loop=&key=part&tool=\" + tool.toUpperCase();\r\n";
   }
   else {
      print "url = \"?create&open=domino&plant=%%PLANT%%&part=\" + part.toUpperCase() + \"&supp=\"+ supp + \"&supplier=\"+ encodeURIComponent(supplier) + \"&loop=&key=part&tool=\" + tool.toUpperCase();\r\n";
   }
   ?>
   table = document.createElement("table");
   table.setAttribute("form", "finalize");
   tr = document.createElement("tr");
   append = ["ID", "Name"];
   for(k in append) {
      th = document.createElement("th");
      th.textContent = append[k];
      tr.appendChild(th);
   }
   table.appendChild(tr);   
   for(k in maintplant) {
      tr = document.createElement("tr");
      td = document.createElement("td");
      td.innerHTML = "<label class=\"radio\"><input onclick=\"execute(this)\" type=\"radio\" name=\"q\" plant=\"" + maintplant[k] + "\" value=\"" + url.replace(/%%PLANT%%/, maintplant[k]) + "\"><span class=\"outer\"><span class=\"inner\"></span></span></label><span style=\"padding-left:27px;\">" + maintplant[k] + "</span>";
      tr.appendChild(td);
      td = document.createElement("td");
      td.textContent = (typeof te.plants[maintplant[k]] == "string") ? unescape(te.plants[maintplant[k]]) : "";
      key = [maintplant[k], td.textContent];
      tr.setAttribute("key", String(key.join(" ")).toLowerCase());
      tr.appendChild(td);
      table.appendChild(tr);
   }
   input = document.createElement("input");
   with(input) {
      value = te.searchstring;
      style.color = "rgb(122,122,122)";
      style.fontStyle = "italic";
      onfocus = function() {
         with(this) {
            value = this.value.replace(new RegExp(te.searchstring, "i"), "");
            style.color = "rgb(0,0,0)";
            style.fontStyle = "normal";
         }
      }
      onkeyup = function() {
         v = String(this.value.toLowerCase()).split(" ");
         tr = document.querySelector("[form='finalize']").getElementsByTagName("tr");
         for(i = 1; i < tr.length; i++) {
            dsp = true;
            for(k in v) {
               if(tr[i].getAttribute("key").indexOf(v[k]) == -1) dsp = false; 
            }
            tr[i].style.display = (dsp) ? "table-row" : "none";
         }
      }
   }   
   document.querySelector("[form='$search.inject']").appendChild(input);
   document.querySelector("[form='$search.inject']").appendChild(table);
}
execute = function() {
   span = document.createElement("span");
   span.style = "display:block; margin-bottom:20px; background:rgba(133,52,56,0.1); padding:8px; width:360px;";
   span.innerHTML = unescape(errmessage[arguments[0].getAttribute("plant")]);
   button = document.createElement("button");
   button.setAttribute("value", arguments[0].value);
   button.textContent = "Proceed";
   button.onclick = function() {
      url = this.value + "&accepted=<?php print rawurlencode(date("Y-m-d H:i:s", microtime(true))); ?>&mtype=" + cache.mtype;
      location.href = url;
      return false;
   }
   with(document.getElementById("msg")) {
      innerHTML = "";
      appendChild(span);
      appendChild(button);
      style.position = "relative";
      style.left = "-127px";
   }
}
</script>
</head>
<body>
<span style="position:relative; top:-29px; left:254px;">&nbsp;>&nbsp;Select&nbsp;plant</span>
<div form="$search.inject" style="position:relative; top:-30px;"></div>
<button style="margin-bottom:22px; margin-left:3px;" onmouseover="this.style.background='rgb(0,102,158)';" onmouseout="this.style.background='rgb(233,131,0)';" type="button" onclick="history.back();">Back</button>
</body>

</html>
<?php
session_start();
require_once("config.php");
ob_start("ob_gzhandler");									
header("Content-Type: text/html; charset=UTF-8"); 
?>

<html>
<head>
<script>
window.onload = function() {
   window.setTimeout(
      function() {
         location.href = "<?php
$part = strtolower(trim(explode(" ", $_REQUEST["part"])[0]));
if(count(explode("-", $part)) == 3) $part = trim(explode("-", $part)[0]."-".intval(explode("-", $part)[1])."-".explode("-", $part)[2]); 
if(substr($part, 0, 2) == "0-") $part = substr($part, 2, strlen($part));
$revision = strtolower(trim(explode(" ", $_REQUEST["part"])[1]));
$tool = trim($_REQUEST["tool"]);
print $_SESSION["remote_domino_path_main"]."/f.cr.rms.start?open=domino&p=add_tool&part=".$part."+".$revision."&orev=_&rev=".$revision."&drw=_&search=".$tool;
?>";
      }, 2000);
}
</script>
</head>
<body>
<img src="../../../../library/images/ajax/ajax-loader.gif">
<span style="font:normal 13px arial, verdana; position:relative; left:4px; top:-3px;">Copying ...</span>
</body>
</html>
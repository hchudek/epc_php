<?php
session_start();
require_once("config.php");
require_once("../../../../library/tools/addin_xml.php");	
ob_start("ob_gzhandler");									
header("Content-Type: text/html; charset=UTF-8"); 
$_SESSION["part"] = $_REQUEST["part"];
?>
<!DOCTYPE html>
<html>
<head>
<?php
print "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://".$_SERVER["SERVER_NAME"]."/library/css5/jquery-ui.css\"></link>\r\n";
print "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://".$_SERVER["SERVER_NAME"]."/library/css5/jquery-ui.theme.css\"></link>\r\n";
print "<link rel=\"stylesheet\" type=\"text/css\" href=\"http://".$_SERVER["SERVER_NAME"]."/library/css5/include3.css?".base64_encode("te.woff")."\"></link>\r\n";  
print "<link rel=\"stylesheet\" type=\"text/css\" href=\"".$_SESSION["database_path"]."css/".basename(__FILE__, ".php").".css\"></link>\r\n";

//print "<script src=\"".$_SESSION["database_path"]."javascript/jqueryui.js\"></script>\r\n";
//additional jquery ui
//$_SESSION["php_server"] + "/apps/epc/ssl/javascript/jload/json.supplier.js");
print "<script src=\"http://".$_SERVER["SERVER_NAME"]."/apps/epc/ssl/javascript/jload/json.supplier.js\"></script>\r\n";
print "<script src=\"".$_SESSION["database_path"]."javascript/basic.js?".basename(__FILE__, ".php")."\"></script>\r\n";
print "<script>\r\n";

foreach($_REQUEST as $k => $v) {
   $o.= "\"".$k."\": \"".urlencode($v)."\", ";
}
print "\$_request = { ".substr($o, 0, strlen($o) - 2)." }\r\n";
print "</script>\r\n";
?>
</head>
<body>
<span style="position:relative; top:-29px; left:260px; font:normal 13px verdana;">&nbsp;>&nbsp;&nbsp;Add&nbsp;tool</span>
<?php
$_REQUEST["part"] = str_replace(" ", "+", $_REQUEST["part"]);
setcookie("DomAuthSessId", $_REQUEST["DomAuthSessId"]);
$file = file_get_authentificated_contents($_SESSION["remote_domino_path_tdt"]."/v.get_part_rev?open&count=9999&restricttocategory=".rawurlencode(strtolower($_REQUEST["part"]))."&function=plain");
if(!strpos("No documents found", $file)) {
   $json = (array)json_decode("[".substr($file, 0, strrpos($file, ","))."]");
}
$json[] = array("unid" => "%%PART%%/b96664afff085571c1257a220048965d", "tool_unid" => "", "Tool number" => "No tool", "Project number" => "", "Case number" => "", "Plant" => "", "Workcenter" => "", "TDT status" => "");

$type = (count($json) > 1) ? "unid" : "part";

$supplier =  file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/parts.php?open&count=1&restricttocategory=".rawurlencode($_REQUEST["part"])."&function=plain");
if(strpos("No documents found", $supplier)){
    $supplier = "";
}
else{
    $supplier = explode("#", trim($supplier))[6];
}

// If supplier is already set by request, take this one
$urlsupplier = rawurldecode($_REQUEST["supplier"]);
if ($urlsupplier != "" ){
    $supplier = $urlsupplier;
}

//Clear the Supplier

$first = strpos($supplier, "[");
$last = strpos($supplier, "]");
if($first ){
    $number = substr($supplier,$first+1,$last-1 );
    $suppliername = substr($supplier,0,$first);
    $supplier = $suppliername."[".ltrim($number, '0');    
    };

$sup = strpos($supplier, "TE00") ? "int":"ext";
print "<table form=\"add_tool\">\r\n";
print "   <tr>\r\n";
print "<div style=\"display: table;\"><span>Search for another Supplier </span><input check=\"supp_check\" style=\"width:500px;\" class=\"ui-autocomplete-input\" name=\"supp\" type=\"\" value=\"".$supplier."\"></div>\r\n";
print "   </tr>\r\n";
print "   <tr>\r\n";
//print "<div form=\"\$search.inject\" include=\"domino\" open=\"".$_REQUEST["open"]."\" path=\"".$_SESSION["database_path"]."\"></div>\r\n";
foreach($json[0] as $k => $v) if(!in_array($k, array("unid", "tool_unid", "TN"))) print "      <th>".str_replace("_", " ", $k)."</th>\r\n";
print "   </tr>\r\n";

print "   <tr>\r\n";
foreach($json as $t) {
   print "   <tr>\r\n";
   foreach($t as $k => $v) {
      if($k == "unid") $unid = explode("/", $v);
      elseif($k == "Tool number") {
         $resin = ($t->tool_unid == "" || substr(trim($v), 0, 1) != "2") ? "1" : file_get_authentificated_contents($_SESSION["remote_domino_path_main"]."/p.resin?open&tool=".$t->tool_unid);
// $resin = 1;
         if($pu == "" && $unid[0] != "") $pu = ($unid[0] == "%%PART%%") ? $_REQUEST["part"] : $unid[0];
         $createdocument = ($unid[0] == "%%PART%%") ? "" : "createdocument&";
         $use_href = ($_REQUEST["open"] == "domino") ? "?".$createdocument."open=domino&p=finalize&sup=".$sup."&supplier=".rawurlencode($supplier)  : "finalize.php?";
         print "      <td tn=\"".trim($t->TN)."\"><a key=\"".trim(str_replace(" ", "/", $_SESSION["part"]." ".trim($t->TN)))."\" plant=\"".trim($t->Plant)."\" supplier=\"".trim($supplier)."\" sup=\"".trim($sup)."\" workcenter=\"".trim($t->Workcenter)."\" unid=\"".trim($t->tool_unid)."\" resin=\"".trim($resin)."\" anchor=\"tool\" href=\"".$use_href."&tool=".$unid[1]."&mtype=".$_REQUEST["mtype"]."&part=".str_replace("%%PART%%", "", str_replace("%%PART%%", $pu."&maintplant=false&t=".$type, $unid[0]))."\">".$v."</a></td>\r\n";
      }
      elseif(!in_array($k, array("tool_unid", "TN"))) print "      <td>".str_replace("ERROR", "&nbsp;", $v)."</td>\r\n";
   }
   print "   </tr>\r\n";
}
print "</table>\r\n";
print "<span>Search for another tool</span><input pu=\"".$pu."\" form=\"\$search.input\" t=\"".$type."\" type=\"text\" value=\"\" onkeydown=\"if(event.keyCode == 13) return false;\" onkeyup=\"\$search.execute(this, false)\">\r\n";
print "<div form=\"\$search.inject\" include=\"domino\" open=\"".$_REQUEST["open"]."\" path=\"".$_SESSION["database_path"]."\"></div>\r\n";
?>
</body>

</html>
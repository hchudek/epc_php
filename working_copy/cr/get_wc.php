<!DOCTYPE html>
<html>
<head>
<style>
body {
   margin:0px;
}
form {
   padding:10px;
   font:normal 12px verdana;
}
table {
   table-layout:fixed;
   border-top:solid 2px rgb(255,255,255);
   border-left:solid 2px rgb(255,255,255);
}
table th {
   font:normal 12px verdana;
   text-align:left;
   border-right:solid 2px rgb(255,255,255);
   border-bottom:solid 2px rgb(255,255,255);
   white-space:nowrap;
   padding:3px;
   height:30px;
   background:rgb(233,131,0);
   color:rgb(255,255,255);
}
table td {
   font:normal 13px verdana;
   background:rgb(242,242,242);
   border-right:solid 2px rgb(255,255,255);
   border-bottom:solid 2px rgb(255,255,255);
   white-space:nowrap;
   padding:3px;
}
</style>
<script>
nest = function(o) {
   table = document.createElement("table");
   table.setAttribute("cellspacing", "0");
   trh = document.createElement("tr");
   table.appendChild(trh);
   for(trow in r) {
      tr = document.createElement("tr");
      table.appendChild(tr);
      for(tdata in r[trow]) {
         if(Number(trow) == 0) {
            th = document.createElement("th");
            th.textContent = tdata;
            trh.appendChild(th);
         }
         td = document.createElement("td");
         td.innerHTML = (o.field == tdata) ? r[trow][tdata].replace(new RegExp(o.q, "gi"), "<b>" + o.q + "</b>")  : r[trow][tdata];
         tr.appendChild(td);
      }
      document.getElementsByTagName("body")[0].appendChild(table);
   }
}
process = function() {
   b = document.getElementsByTagName("body")[0];
   b.innerHTML += "<span style=\"font:normal 13px verdana; display:inline-block;margin:11px;\"><span style=\"position:relative; top:-8px;\">Processing ...</span><img src=\"data:image/gif;base64,<?php $file = file_get_contents("../../../../library/images/ajax/ajax-loader2.gif"); print base64_encode($file); ?>\" style=\"position:relative; left:7px; top:2px;\"></span>";
   v = document.getElementsByName("rownum")[0];
   with(document.getElementsByTagName("form")[0]) {
      style.display = "none";
      submit();
   }
}
</script>

</head>
<body>
<?php
ini_set("memory_limit", "2048M");
 
if(!isset($_POST) || count($_POST) < 1) {
   print "<form method=\"post\" action=\"?".$_SERVER["QUERY_STRING"]."\">\r\n";
   print "<select onchange=\"document.getElementsByName('rownum')[0].value = this[this.selectedIndex].text\">\r\n";
   print "<option>10</option>\r\n";
   print "<option>20</option>\r\n";
   print "<option>50</option>\r\n";
   print "<option>100</option>\r\n";
   print "<option>1000</option>\r\n";
   print "<option>10000</option>\r\n";
   print "<option>100000</option>\r\n";
   print "<option>1000000</option>\r\n";
   print "</select>\r\n";
   print "<select name=\"field\">\r\n";
   print "<option value=\"EQUNR\">EQUNR</option>\r\n";
   print "</select>\r\n";
   print "<input type=\"text\" value=\"".$_SERVER["QUERY_STRING"]."\" name=\"q\" autocomplete=\"off\" onkeyup=\"document.getElementsByTagName('form')[0].action='?'+escape(this.value.trim())\">\r\n";
   print "<input type=\"hidden\" name=\"rownum\" value=\"10\">\r\n";
   print "<button type=\"button\" onclick=\"process();\">Submit</button>\r\n";
   print "</form>\r\n";
}
else {
   $db = new PDO("oci:dbname=ADW_ADHOC", "eg046555", "A24\$3009m907");
   $_POST["q"] = $_SERVER["QUERY_STRING"];
   $query = "SELECT * FROM EQUIPMENT_MAIN_DATA WHERE ".trim($_POST["field"])." LIKE '%".trim($_POST["q"])."%' AND ROWNUM <= ".$_POST["rownum"];
   $sth = $db->prepare($query);
   $sth->execute();

   $count = 0;
   while($result = $sth->fetch(PDO::FETCH_ASSOC)) {
      foreach($result as $k => $v) if($k != "ROWNUM") {
         $res[$count][$k] = utf8_encode($v);
      }
      print "\r\n";
     $count++;
   }
   print "<script>\r\n";
   print "r = ".json_encode($res).";\r\n";
   print "nest({\"field\": \"".$_POST["field"]."\", \"q\": \"".$_POST["q"]."\"});";
   print "</script>\r\n";
}

?>

</body>
</htmL>
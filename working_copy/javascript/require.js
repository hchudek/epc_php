<?php header("Content-type: text/javascript"); ob_start("ob_gzhandler");

$include = explode(";", rawurldecode($_SERVER["QUERY_STRING"]));

print "/*\r\n";
foreach($include as $k => $v) {
   if(!file_exists($v.".js")) unset($include[$k]);
   else print substr($v.".js                               ", 0, 32).substr("        ".filesize($v.".js"), -8)."\r\n";
}
print "*/\r\n";
foreach($include as $v) {
   print "\r\n/* ".$v.".js */\r\n";
   include_once($v.".js");
}

?>